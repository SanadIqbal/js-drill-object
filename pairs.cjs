const data = require('./data.cjs');
function pairs(data) {
    if (arguments.length !== 1 || typeof data !== 'object') {
        return [];
    }

    const result = [];
    for (let key in data) {
        const list = [];
        list.push(key);
        list.push(data[key]);
        result.push(list);
    }
    return result;
}

module.exports=pairs;