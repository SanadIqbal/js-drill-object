function keys(data){
    if(arguments.length !==1 || typeof data !=='object')
    {
    return [];
    }
    const result = [];
    for(let key in data)
    {
    result.push(key);
    }
    return result;
}

module.exports = keys;
