const data = require('./data.cjs');

function defaults(data, defaultProps) {
    if (arguments.length !== 2 || typeof data !== 'object' || typeof defaultProps !== 'object') {
        console.log(typeof defaultProps);
        return {};
    }

    for (let key in defaultProps) {
        if (data[key] === undefined) {
            data[key] = defaultProps[key];
        }
    }
    return data;
}

module.exports = defaults;