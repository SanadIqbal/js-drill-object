const data = require('./data.cjs');

function mapObject(data, cb) {
    if (arguments.length !== 2 || typeof data !== 'object') {
        return [];
    }
    const result = {};
    for (let key in data) {
        result[key] = cb(key, data);
    }
    return result;
}

module.exports=mapObject;