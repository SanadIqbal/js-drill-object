const data = require('./data.cjs');

function invert(data) {
    if (arguments.length !== 1 || typeof data !== 'object') {
        return {};
    }
    const result = {};
    for (let key in data) {
        result[data[key]] = key;
    }
    return result;
}

module.exports = invert;