const data = require('./data.cjs');
function values(data) {
    if (arguments.length !== 1 || typeof data !== 'object') {
        return [];
    }

    const result = [];
    for (let key in data) {
        result.push(data[key]);
    }

    return result;
}

module.exports = values ;
console.log(values(data));